#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""
Logger class and LoggerFactory class
LoggerFactory - Assembles Logger class and returns it
Logger - Logs program's output into specified file
"""

import logging
from logging.handlers import SocketHandler

CRITICAL: int = logging.CRITICAL
WARNING: int = logging.WARNING
INFO: int = logging.INFO
DEBUG: int = logging.DEBUG


class Logger:
    """
    Logger class
    :param self.logger: Logger instance
    :type self.logger: logging.Logger
    :param self.message_prefix: Message prefix
    :type self.message_prefix: str
    """

    def __init__(self, name: str, level: int, logging_format: str, time_format: str, message_prefix: str,
                 socket_handler: SocketHandler = None) -> None:
        """
        Logger class init
        :param name: Name of logger file
        :type name: str
        :param level: Minimum level which is visible
        :type level: int
        :param logging_format: Format of log messages
        :type logging_format: str
        :param time_format: Format of time in log messages
        :type time_format: str
        :param message_prefix: Message prefix
        :type message_prefix: str
        :return: None
        :rtype: NoneType
        """
        self.logger: logging.Logger = logging.getLogger(__name__)
        self.logger.setLevel(level)

        fh = logging.FileHandler(name)
        fh_format = logging.Formatter(logging_format, time_format)
        fh.setFormatter(fh_format)

        if socket_handler is not None:
            self.logger.addHandler(socket_handler)

        self.logger.addHandler(fh)
        self.message_prefix: str = message_prefix

    def log(self, message: str, level: int=INFO) -> None:
        """
        Logs messages with specifies level
        :param message: Message to log
        :type message: str
        :param level: Level of message
        :type level: int
        :return: None
        :rtype: NoneType
        """
        message = self.message_prefix + " - " + message

        if level == DEBUG:
            self.logger.debug(message)
        elif level == INFO:
            self.logger.info(message)
        elif level == WARNING:
            self.logger.warning(message)
        elif level == CRITICAL:
            self.logger.critical(message)

    def shutdown(self) -> None:
        """
        Shutdowns logger
        :return: None
        :rtype: NoneType
        """
        logging.shutdown()


class LoggerFactory:
    """
    LoggerFactory class
    :param self.name: Name of logger file
    :type self.name: str
    :param self.level: Minimum level which is visible
    :type self.level: int
    :param self.logging_format: Format of log messages
    :type self.logging_format: str
    :param self.time_format: Format of time in log messages
    :type self.time_format: str
    :param self.message_prefix: Message prefix
    :type self.message_prefix: str
    """

    def __init__(self) -> None:
        """
        LoggerFactory class init
        :return: None
        :rtype: NoneType
        """
        self.name: str = "script.log"
        self.level: int = DEBUG
        self.logging_format: str = "[%(asctime)s][%(levelname)s]: %(message)s"
        self.time_format: str = "%Y-%m-%d %H:%M:%S"
        self.message_prefix = ""
        self.socket_handler = None

    def set_name(self, name: str) -> 'LoggerFactory':
        """
        Sets name of logger file
        :param name: Name of logger file
        :type name: str
        :return: Returns self
        :rtype: LoggerFactory
        """
        self.name = name
        return self

    def set_logging_level(self, level: int) -> 'LoggerFactory':
        """
        Sets minimum logging level
        :param level: Minimum level which is visible
        :type level: int
        :return: Returns self
        :rtype: LoggerFactory
        """
        self.level = level
        return self

    def set_logging_format(self, logging_format: str) -> 'LoggerFactory':
        """
        Sets logging format
        :param logging_format: Format of log messages
        :type logging_format: str
        :return: Returns self
        :rtype: LoggerFactory
        """
        self.logging_format = logging_format
        return self

    def set_time_format(self, time_format: str) -> 'LoggerFactory':
        """
        Sets time format
        :param time_format: Format of time in log messages
        :type time_format: str
        :return: Returns self
        :rtype: LoggerFactory
        """
        self.time_format = time_format
        return self

    def set_message_prefix(self, prefix: str) -> 'LoggerFactory':
        """
        Sets message prefix
        :param prefix: Message prefix
        :type prefix: str
        :return: Returns self
        :rtype: LoggerFactory
        """
        self.message_prefix = prefix
        return self

    def add_socket_handler(self, host: str, port: int):
        self.socket_handler = SocketHandler(host, port)
        return self

    def build(self) -> Logger:
        """
        Builds Logger class
        :return: Returns Logger class
        :rtype: Logger
        """
        return Logger(self.name, self.level, self.logging_format, self.time_format, self.message_prefix, self.socket_handler)

