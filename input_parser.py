#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""
InputParser class and InputParserFactory class
InputParserFactory - Assembles InputParser class and returns it
InputParser - Parses user's input and returns its content
"""

import getopt
import sys
from typing import Dict, List, Tuple, Union

from .exceptions import ParameterException


class InputParser:
    """
    InputParser class
    :param self.values: Values used to parse user's input
    :type self.values: List[Dict[Tuple[str, str], Union[str, bool]]], default None
    :param self.argv: Argv from command line
    :type self.argv: List[str]
    :param self.possible_opts: All possible opts
    :type self.possible_opts: Dict[str, Union[str, List[str]]
    """

    def __init__(self, values: List[Dict[Tuple[str, str], Union[str, bool]]]) -> None:
        """
        InputParser class init
        :param values: Expected values from CLI
        :type values: List[Dict[Tuple[str, str], Union[str, bool]]]
        :return: None
        :rtype: NoneType
        """
        self.values: List[Dict[Tuple[str, str], Union[str, bool]]] = values
        self.argv: List[str] = sys.argv[1:]
        self.possible_opts: Dict[str, Union[str, List[str]]] = self._get_possible_opts()

    def parse(self) -> None:
        """
        Parses CLI inputs
        :return: None
        :rtype: NoneType
        """
        try:
            opts, args = getopt.getopt(self.argv, self.possible_opts["short"], self.possible_opts["long"])
        except getopt.GetoptError as e:
            print("Option -" + e.opt + " not recognised")
            sys.exit()

        for separate_dict in self.values:
            [(possible_opts, default)] = separate_dict.items()
            for opt, arg in opts:
                if default is True or default is False and opt in possible_opts:
                    separate_dict[possible_opts] = not default
                elif opt in possible_opts:
                    separate_dict[possible_opts] = arg

    def get_values_original(self) -> List[Dict[Tuple[str, str], Union[str, bool]]]:
        """
        Gets original values
        :return: Returns original raw values
        :rtype: List[Dict[Tuple[str, str], Union[str, bool]]]
        """
        return self.values

    def get_values_dict(self) -> Dict[str, Union[str, bool]]:
        """
        Gets values in dict with longopt as key
        :return: Returns values transcribed to dict
        :rtype: Dict[str, Union[str, bool]]
        """
        new_dict: Dict[str, Union[str, bool]] = {}
        for separate_dict in self.values:
            [(possible_opts, default)] = separate_dict.items()
            new_dict.update({self._sort_opts(possible_opts)["long"].replace("--", ""): default})
        return new_dict

    def _sort_opts(self, opts: Tuple[str, str]) -> Dict[str, str]:
        """
        Sorts opt to dict
        :param opts: Unsorted opts
        :type opts: Tuple[str, str]
        :return: Returns Sorted opts
        :rtype: Dict[str, str]
        """
        opts: List[str] = list(opts)
        return {"short": opts[0], "long": opts[1]} if opts[1].startswith("--") else {"short": opts[1], "long": opts[0]}

    def _get_possible_opts(self) -> Dict[str, Union[str, List[str]]]:
        """
        Gets all possible opts prepared for getopt.getopt function
        :return: Returns opts in dict
        :rtype: Dict[str, Union[str, List[str]]]
        """
        shortopts: str = ""
        longopts: List[str] = []

        for separate_dict in self.values:
            [(possible_opts, default)] = separate_dict.items()
            opts: Dict[str, str] = self._sort_opts(possible_opts)

            shortopts += opts["short"].replace("-", "")
            longopts.append(opts["long"].replace("--", ""))

            if not (default is True or default is False):
                shortopts += ":"
                longopts[len(longopts) - 1] += "="

        return {
            "short": shortopts,
            "long": longopts
        }


class InputParserFactory:
    """
    InputParserFactory class
    :param self.values: Values used to parse user's input
    :type self.values: List[Dict[Tuple[str, str], Union[str, bool]]]
    """

    def __init__(self) -> None:
        """
        InputParserFactory init
        :return: None
        :rtype: NoneType
        """
        self.values: List[Dict[Tuple[str, str], Union[str, bool]]] = []

    def add_values(self, values: List[Dict[Tuple[str, str], Union[str, bool]]] = None) -> 'InputParserFactory':
        """
        Adds values from user to self.values
        :param values: User's values used to parse input
        :type values: List[Dict[Tuple[str, str], Union[str, bool]]]
        :return: Returns self
        :rtype: InputParserFactory
        """
        if values is not None:
            self.values += values
        return self

    def build(self) -> InputParser:
        """
        Builds InputParser class
        :return: Returns InputParser class
        :rtype: InputParser
        """
        if not self.values:
            raise ParameterException("No parameters speficied")
        else:
            return InputParser(self.values)
