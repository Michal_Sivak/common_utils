#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""
Custom exception classes.

"""


class HTTPMethodException(Exception):
    """Wrong method entered."""


class ScrapingException(Exception):
    """Problem occurred when scraping website."""


class NoImplementationException(Exception):
    """Something is not implemented."""


class ParameterException(Exception):
    """Parameter is not assigned."""


class InputException(Exception):
    """User's input is not complete."""
