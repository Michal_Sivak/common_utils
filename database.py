#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""
Database class
"""

from typing import List, Dict

import sqlite3


class Database:
    def __init__(self, connection) -> None:
        self.database_connection = connection
        self.database_cursor = connection.cursor()

    def query_one(self, query: str, parameters: Dict[str, str]) -> List[str]:
        pass

    def query_all(self, query: str, parameters: Dict[str, str]) -> List[str]:
        pass

    def query(self, query: str, parameters: Dict[str, str]) -> None:
        pass

    def commit(self) -> None:
        pass

    def close(self) -> None:
        pass


class SQLiteDatabase(Database):
    def __init__(self, name: str) -> None:
        connection = sqlite3.connect(name)
        connection.row_factory = lambda cursor, row: list(row)
        super().__init__(connection)

    def query_one(self, query: str, parameters: Dict[str, str]) -> List[str]:
        return self.database_cursor.execute(query, parameters).fetchone()

    def query_all(self, query: str, parameters: Dict[str, str]) -> List[str]:
        return self.database_cursor.execute(query, parameters).fetchall()

    def query(self, query: str, parameters: Dict[str, str]) -> None:
        self.database_cursor.execute(query, parameters)
        self.commit()

    def commit(self) -> None:
        self.database_connection.commit()

    def close(self) -> None:
        self.database_connection.close()
