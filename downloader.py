#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""
Downloader class and Downloader factory class
DownloaderFactory - Assembles Downloader class and returns it
Downloader - Downloades page and returns its content from url
"""

import random
import csv
from typing import List, Dict

import requests
import urllib3

from .exceptions import HTTPMethodException
from .logger import Logger, CRITICAL, WARNING, INFO, DEBUG

from ftfy import fix_encoding


class Downloader:
    """
    Downloader class
    :param self.proxies: Usable proxies for requesting urls
    :type self.proxies: List[Dict[str, str]]
    :param self.headers: Headers send with request
    :type self.headers: Dict[str, str]
    :param self.logger: Logs script's output
    :type self.logger: Logger
    """

    def __init__(self, proxies: List[Dict[str, str]], headers: Dict[str, str], disable_warnings: bool,
                 logger: Logger) -> None:
        """
        Downloader class init
        :param proxies: List of all available proxies
        :type proxies: List[Dict[str, str]
        :param headers: Headers, default {"cache-control": "private", "connection": "close"}
        :type headers: Dict[str, str]
        :param disable_warnings: Disables urllib3 warnings, default True
        :type disable_warnings: bool
        :param logger: Logs script's output
        :type logger: Logger
        :return: None
        :rtype: NoneType
        """
        self.proxies: List[Dict[str, str]] = proxies if proxies != [] else [{}]
        self.headers: Dict[str, str] = headers
        self.logger = logger

        if self.proxies == [{}]:
            self._log("Proxy list passed to downloader is empty", WARNING)

        if disable_warnings:
            self._disable_warnings()

    def download_page(self, url: str,
                      timeout: int = 2,
                      timeout_max_count: int = 3,
                      enable_proxies: bool = True,
                      method: str = "GET",
                      content: str = "",
                      headers: Dict[str, str] = {}) -> str:
        """
        Downloads a page from the web.
        :param headers: Request headers specific to this requests. Will get added to instance headers.
        :param url: The URL to make request to
        :type url: str
        :param timeout: Request timeout in seconds, default 2
        :type timeout: int
        :param timeout_max_count: How many times downloader tries to get response if timeout occurres
        :type timeout_max_count: int
        :param enable_proxies: Enables proxies, default True
        :type enable_proxies: bool
        :param method: Specified method to use, default "GET"
        :type method: str
        :param content: Content of POST request, default ""
        :type content: str
        :raise HTTPMethodException: When HTTP method is not GET of POST, this exception raises
        :return: Returns response content or "" if the request times out (or if there is 404 response code)
        :rtype: str
        """

        if method.upper() != "GET" and method.upper() != "POST":
            raise HTTPMethodException("Method needs to be GET or POST, your is: " + method.upper())

        headers = {**self.headers, **headers}

        response = None
        proxy = None
        timeout_count = 0

        while response is None:
            try:
                proxy = self._get_random_proxy() if enable_proxies else {}
                if method.upper() == "GET":
                    response = requests.get(url, verify=False, timeout=timeout, headers=headers, proxies=proxy)
                if method.upper() == "POST":
                    response = requests.post(url, verify=False, timeout=timeout, headers=headers, proxies=proxy,
                                             data=content)
                response.raise_for_status()
                if response.status_code == 200:
                    self._log("URL (" + url + ") successfully downloaded, code 200, method " + method +
                              ", proxy " + str(proxy), INFO)
                    break
            except KeyboardInterrupt:
                self._log("User interrupted script", CRITICAL)
                break
            except requests.HTTPError:
                self._log("URL (" + url + ") not downloaded, code 404, method " + method +
                          ", proxy " + str(proxy), WARNING)
                return ""
            except requests.ConnectionError:
                self._log("URL (" + url + ") not downloaded, code 504, method " + method +
                          ", proxy " + str(proxy), WARNING)
                return ""
            except requests.Timeout:
                self._log("URL (" + url + ") not downloaded, code 504, method " + method +
                          ", proxy " + str(proxy), WARNING)
                timeout_count += 1
                if timeout_count > timeout_max_count:
                    return ""

        return fix_encoding(response.text)

    def _get_random_proxy(self) -> Dict[str, str]:
        """
        Gets a random proxy.
        :return: Returns one proxy dictionary from self.proxies
        :rtype: Dict[str, str]
        """
        return self.proxies[random.randint(0, len(self.proxies) - 1)]

    def _disable_warnings(self) -> None:
        """
        Disables urllib3 warnings.
        :return: None
        :rtype: NoneType
        """
        self._log("Warnings disabled", INFO)
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    def _log(self, message: str, level: int) -> None:
        """
        A simple logging function.
        :param message: Message to log
        :type message: str
        :param level: Level of message
        :type level: int
        :return: None
        :rtype: NoneType
        """
        if self.logger is not None:
            self.logger.log(message, level)


class DownloaderFactory:
    """
    DownloaderFactory class
    :param self.files: Paths to all proxy files.
    :type self.files: List[List[str]]
    :param self.no_warnings: Disable warnings, default False
    :type self.no_warnings: bool
    :param self.headers: Headers sent with each request, default {"cache-control": "private", "connection": "close"}
    :type self.headers: Dict[str, str]
    :param self.proxies: All available proxies (from all files combined)
    :type self.proxies: List[Dict[str, str]]
    :param self.txt_splitters: Splitters for each proxy file.
    :type self.txt_splitters: Dict[str, str]
    :param self.credentials: Credentials for the proxies.
    :type self.credentials: Dict[str, Dict[str, str]]
    :param self.logger: A logger for logging downloads.
    :type self.logger: Logger
    """

    def __init__(self) -> None:
        """
        DownloaderFactory class init
        :return: None
        :rtype: NoneType
        """
        self.files: List[List[str]] = []
        self.no_warnings: bool = False
        self.headers: Dict[str, str] = {"cache-control": "private", "connection": "close"}
        self.proxies: List[Dict[str, str]] = []
        self.txt_splitters: Dict[str, str] = {}
        self.credentials: Dict[str, Dict[str, str]] = {}
        self.logger: Logger = None

    def load_proxies(self, file: str = "hardcoded_files/proxies.csv", protocol: str = "SOCKS5",
                     splitter: str = ";", name: str = None, password: str = None) -> 'DownloaderFactory':
        """
        Loads proxies from the specified file.
        :param file: File from which proxies are load, default "hardcoded_files/proxies.csv"
        :type file: str
        :param protocol: Proxies protocol used with specified file, default "SOCKS5"
        :type protocol: str
        :param splitter: Text file splitter, only used with text files, default ";"
        :type splitter: str
        :param name: User's name used with proxies, default None
        :type name: str
        :param password: User's password used with proxies, default None
        :type password: str
        :return: Returns self
        :rtype: DownloaderFactory
        """
        if not any(path == file for path, used_protocol in self.files):
            self.files.append([file, protocol])
        if ".txt" in file.lower():
            self.txt_splitters.update({file: splitter})
        if name is not None and password is not None:
            self.credentials.update({file: {"name": name, "password": password}})
        return self

    def disable_warnings(self) -> 'DownloaderFactory':
        """
        Disables urllib3 warnings
        :return: Returns self
        :rtype: DownloaderFactory
        """
        self.no_warnings = True
        return self

    def add_headers(self, headers: Dict[str, str]) -> 'DownloaderFactory':
        """
        Adds headers sent with each request
        :param headers: Headers send with request
        :return: Returns self
        :rtype: DownloaderFactory
        """
        self.headers = headers
        return self

    def add_logger(self, logger: Logger) -> 'DownloaderFactory':
        """
        Adds a logger to the Downloader instance.
        :param logger: Logger used to log script's output
        :type logger: Logger
        :return: Returns self
        :rtype: DownloaderFactory
        """
        self.logger = logger
        return self

    def build(self) -> Downloader:
        """
        Builds a Downloader class instance.
        :return: Returns Downloader class
        :rtype: Downloader
        """
        self.proxies += [proxy_list for file, protocol in self.files for proxy_list in
                         self._get_proxies(file, protocol)]
        return Downloader(self.proxies, self.headers, self.no_warnings, self.logger)

    def _get_proxies(self, file: str, protocol: str) -> List[Dict[str, str]]:
        """
        Gets all available proxies.
        :param file: Path to file from which proxies are loaded
        :type file: str
        :param protocol: Proxies protocol used with specified file
        :return: List of all available proxies in one file
        :rtype: List[Dict[str, str]
        """

        def assemble_proxies(ip: str, port: str, name: str = "", password: str = "") -> Dict[str, str]:
            """
            Assembles a dict that binds proxies to https and http protocols in the requests library.
            :param ip: IP address of proxy server
            :type ip: str
            :param port: Port of proxy server
            :type port: str
            :param name: User's name used to connect to server
            :type name: str
            :param password: User's password used to connect to server
            :type password: str
            :return: Returns assembled proxy dict
            :rtype: Dict[str, str]
            """
            credentials_string: str = (name + ":" + password + "@") if name != "" and password != "" else ""
            return {
                "http": (protocol.lower() + "://" + credentials_string + ip + ":" + port.replace("\n", "")),
                "https": (protocol.lower() + "://" + credentials_string + ip + ":" + port.replace("\n", ""))
            }

        def load_from_file(txt_name: str, splitter: str) -> List[List[str]]:
            """
            Reads proxies from a text file.
            :param txt_name: File name from which are proxies loaded
            :type txt_name: str
            :param splitter: Splitter used for division ip and port
            :type splitter: str
            :return: List of all available proxies from specified file
            :rtype: List[List[str]]
            """
            with open(txt_name, 'r') as f:
                return [[line.split(splitter)[0], line.split(splitter)[1]] for line in f.readlines()]

        proxies: List[List[str]] = []
        if ".csv" in file.lower():
            proxies = load_from_file(file, ",")
        elif ".txt" in file.lower():
            proxies = load_from_file(file, self.txt_splitters[file])

        if file not in self.credentials:
            return [assemble_proxies(ip, port) for ip, port in proxies]
        else:
            credentials: Dict[str, str] = self.credentials[file]
            return [assemble_proxies(ip, port, credentials["name"], credentials["password"]) for ip, port in proxies]
